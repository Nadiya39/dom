class Character {
    constructor(id, created, species, img, episodes, name, location) {
        this.id = id;
        this.created = created;
        this.species = species;
        this.img = img;
        this.episodes = episodes;
        this.name = name;
        this.location = location;
      }
}

const sortingOptions = {
    dateAsc: 'date-asc',
    dateDesc: 'date-desc',
    episodes: 'episodes'
};

const list = document.getElementById('characters-table-id');
const sortingSelect = document.getElementById('sorting-select-id');
const scrollUpButton = document.getElementById('button-scroll-up-id');
const resetButton = document.getElementById('button-reset-id');

const characters = [];
let filteredCharacters = [];

let nextPage = 1;

load_info(nextPage).then(data => {
    characters.push(...data);
    addCharacters(data);
});


window.addEventListener('scroll', (event) => {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        load_info(nextPage).then(data => {
            addCharacters(data);
        });
    }

    setScrollUpButtonVisibility(window.scrollY > window.innerHeight);
});

sortingSelect.addEventListener('change', () => {
    switch(sortingSelect.value) {
        case sortingOptions.dateAsc:
            filteredCharacters.sort(sortCharactersByDateAsc);
            break;
        case sortingOptions.dateDesc:
            filteredCharacters.sort(sortCharactersByDateDesc);
            break;
        case sortingOptions.episodes:
            filteredCharacters.sort(sortCharactersByEpisodes);
            break;
        default:
            console.error('No such sorting functionality');
            return;
    }

    emptyTable();
    filteredCharacters.forEach(create_character);
});

scrollUpButton.addEventListener('click', () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
});

resetButton.addEventListener('click', () => {
    emptyTable();
    characters.forEach(create_character);
    filteredCharacters = [ ...characters ];
    nextPage = 2;
});

function addCharacters(data) {
    filteredCharacters.push(...data);
    data.map(create_character);
    nextPage += 1;
}

function sortCharactersByEpisodes(element1, element2) {
    if (element1.episodes.length < element2.episodes.length) {
        return 1;
    }
    if (element1.episodes.length > element2.episodes.length) {
        return -1;
    }

    return sortCharactersByDateDesc(element1, element2);
};

function sortCharactersByDateDesc(element1, element2) {
    const createdDate1 = new Date(element1.created);
    const createdDate2 = new Date(element2.created);
    
    if (createdDate1 < createdDate2) {
        return 1;
    }
    if (createdDate1 > createdDate2) {
        return -1;
    }

    return 0;
};

function sortCharactersByDateAsc(element1, element2) {
    const createdDate1 = new Date(element1.created);
    const createdDate2 = new Date(element2.created);
    
    if (createdDate1 > createdDate2) {
        return 1;
    }
    if (createdDate1 < createdDate2) {
        return -1;
    }

    return 0;
};



function load_info(nextPage) {
    const url = 'https://rickandmortyapi.com/api/character?page=' + nextPage;
    console.log(url);
    return fetch(url)
        .then(res => res.json())
        .then(data => data.results.map(character => {
            return new Character(
                character.id,
                character.created,
                character.species,
                character.image,
                character.episode,
                character.name,
                character.location
            );
        }))
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
};

function get_episodes_text(episodes) {
    return episodes.map(episode => episode.split('/')[5]).join(', ');
}

function create_character(element) {
    const character = document.createElement('tr');
    character.className = 'character';
    character.id = element.id;

    const avatar_td = document.createElement('td');
    const avatar = document.createElement('img');
    avatar.className = 'avatar';
    avatar.src = element.img;
    avatar.alt = element.name;
    avatar_td.appendChild(avatar);
    character.appendChild(avatar_td);

    const info_td = document.createElement('td');
    const info = document.createElement('ul');
    info.className = 'info';


    const name = document.createElement('li');
    name.className = 'name';
    name.textContent = element.name
    info.appendChild(name);

    const species = document.createElement('li');
    species.className = 'species';
    species.textContent = element.species
    info.appendChild(species);

    const location = document.createElement('li');
    location.className = 'location';
    location.textContent = element.location.name
    info.appendChild(location);

    const episodes = document.createElement('li');
    episodes.className = 'episodes';
    episodes.textContent = get_episodes_text(element.episodes)
    info.appendChild(episodes);

    const created = document.createElement('li');
    const created_date = new Date(element.created);
    created.className = 'created';
    created.textContent = created_date.toDateString() + " " + created_date.toLocaleTimeString();
    info.appendChild(created);

    info_td.appendChild(info);
    character.appendChild(info_td);

    const delete_td = document.createElement('td');
    const delete_button = document.createElement('input');
    delete_button.type = 'button';
    delete_button.value = 'Delete';
    delete_button.addEventListener('click', () => {
        removeCharacter(element.id);
    });

    delete_td.appendChild(delete_button);

    character.appendChild(delete_td);

    list.appendChild(character);
};

function removeCharacter(id) {
    for(let i = 0; i < filteredCharacters.length; i++){
        if (filteredCharacters[i].id === id) {
            filteredCharacters.splice(i, 1);
            document.getElementById(id).remove();
            break;
        }
    }
}

function emptyTable() {
    while (list.firstChild) {
        list.removeChild(list.firstChild);
    }
};

function setScrollUpButtonVisibility(toDisplay) {
    scrollUpButton.style.display = toDisplay ? 'block' : 'none';
}
